// Import the Fragment component from reaact
//import { Fragment } from 'react';
import {useState} from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Switch } from 'react-router-dom';
import AppNavbar from './components/AppNavbar';
// import Banner from './components/Banner';
// import Highlights from './components/Highlights';
import Courses from './pages/Courses';
import Error from './pages/Error';
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import './App.css';
import { UserProvider } from './UserContext';

function App() {
  // State hook for the user state that's defined for aglobal scope
  // Initialize an object with properties from the localstorage
  const [user, setUser] = useState({
    email: localStorage.getItem('email')
  })

  // Function for clearing localstorage on Logout
  const unsetUser = () => {
    localStorage.clear();
  }

  return (
    <UserProvider value={{user,setUser, unsetUser}}>
      <Router>
         <AppNavbar />
         <Container>
           <Switch>
             <Route exact path="/" component={Home} />
             <Route exact path="/courses" component={Courses} />
             <Route exact path="/login" component={Login} />
             <Route exact path="/logout" component={Logout} />
             <Route exact path="/register" component={Register} />
             <Route component={Error} />
           </Switch>
         </Container>
       </Router>
    </UserProvider>

  );
}

export default App;
