// import state hook from readt
import { useState, useEffect } from 'react';
// import Card from 'react-bootstrap/Card';
// import Button from 'react-bootstrap/Button';
import { Card, Button } from 'react-bootstrap';

export default function CourseCard({courseProp}) {
    // Checks to see if the data was successfully passed
    // console.log(props);
    // console.log(typeof props);
    const {name, description, price} = courseProp;

    // use state hook in this component to be able to store its state 
    //  Satte are use to keep track of information related to individual components
    /* Syntax:
		const [getter, setter] = useState(initialGetterValue);
    */

    const [count, setCount] = useState(0);
    const [seats, setSeats] = useState(30);
    const [isOpen, setIsOpen] = useState(false)

    console.log(useState(0));

    // Function to keep track of the enrolles for a course
    function enroll(){
    	setCount(count + 1);
    	console.log('Enrollees: ' + count)
    	setSeats(seats - 1)
    	console.log('Seats: ' + seats)
    }
/*    function enroll(){
    	//to increment the setter use a +1
    	//setCount(count - 1);
    	console.log('Enrollees: ' + count);
    	if(countSeat > 0){
    		setCount(count + 1);
    		setCountSeat(countSeat - 1);
    	}else{
    		alert('No more seats available')
    	}
    }*/
    console.log('Seats:' - count);

    // Define a useEffect hook to have the CourseCard component perform a certain task after every DOM update
    useEffect(()=> {
    	if(seats === 0){
    		setIsOpen(true);
    	}
    }, [seats]) 

    return (
       <Card className="mb-2">
           <Card.Body>
               <Card.Title>{name}</Card.Title>
               <Card.Subtitle>Description:</Card.Subtitle>
               <Card.Text>{description}</Card.Text>
               <Card.Subtitle>Price:</Card.Subtitle>
               <Card.Text>PhP {price}</Card.Text>
               <Card.Text>Enrollees: {count}</Card.Text>
               <Card.Text>Seats: {seats}</Card.Text>
               <Button variant="primary" onClick={enroll} disabled={isOpen}>Enroll</Button>
           </Card.Body>
       </Card> 
    )

}
