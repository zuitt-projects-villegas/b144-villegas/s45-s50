import { Fragment } from 'react';
import CourseCard from '../components/CourseCard';
import courses from '../data/courses';

export default function Courses(){
	// Checks to see if mock data was captured
	// console.log(courses)
	// console.log(courses[0])

	const coursesData = courses.map(course => {
		return (
			<CourseCard key = {course.id} courseProp={course} />
		)
	})

	return(
		<Fragment>
			{coursesData}
		</Fragment>
	)
}