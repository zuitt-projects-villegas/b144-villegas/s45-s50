import { useState,useEffect, useContext } from 'react';
import { Form, Button} from 'react-bootstrap';
import { Redirect} from 'react-router-dom';
import UserContext from '../UserContext';

export default function Register(){

	const { user, setUser } = useContext(UserContext);
	
	// State hooks to store the values of the input fields
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	// State to determine whether sumbit button is enabled or not
	const [isActive, setIsActive] = useState(false)

	// Chekcs if value are successfully binded
	console.log(email);
	console.log(password1);
	console.log(password2);

	// Function to simulate user regstration

	function registerUser(e) {
		// Prevent page redirection via form submission
		e.preventDefault();

		// Clear input
		setEmail('');
		setPassword1('');
		setPassword2('');

		alert('Thank you for registering!')
	}

	useEffect(() => {
		// Validation to enable the sumbit button when all fields are populated and both passwords match
		if((email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
			setIsActive(true)
		}else{
			setIsActive(false)
		}
	}, [email, password1, password2])

	return(
		(user.email !== null) ?
		    <Redirect to="/courses" />
		    
		    :
		<Form onSubmit={(e) => registerUser(e)}>
		  <Form.Group className="mb-3" controlId="userEmail">
		    <Form.Label>Email address</Form.Label>
		    <Form.Control 
		    	type="email" 
		    	placeholder="Enter email"
		    	value = {email} 
		    	onChange = { e => setEmail(e.target.value)} 
		    	required 
		    />
		    <Form.Text className="text-muted">
		      We'll never share your email with anyone else.
		    </Form.Text>
		  </Form.Group>

		  <Form.Group className="mb-3" controlId="password1">
		    <Form.Label>Password</Form.Label>
		    <Form.Control 
		    	type="password" 
		    	placeholder="Password" 
		    	value = {password1} 
		    	onChange = { e => setPassword1(e.target.value)} 
		    	required 
		    />
		  </Form.Group>

		  <Form.Group className="mb-3" controlId="password2">
		    <Form.Label>Verify Password</Form.Label>
		    <Form.Control 
		    	type="password" 
		    	placeholder="Verify Password"
		    	value = {password2}
		    	onChange = { e => setPassword2(e.target.value)} 
		    	required
		    />
		  </Form.Group>
		  {/*Condistionally render submit button based in isActive state*/}
		  { isActive ?
		  	<Button variant="primary" type="submit" id="sumbitBtn">
		  	  Submit
		  	</Button>
		  	:
		  	<Button variant="primary" type="submit" id="sumbitBtn" disabled>
		  	  Submit
		  	</Button>
		  }
	
		</Form>
	)
}
